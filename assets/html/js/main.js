function android_getInput() {
  console.log("Get Input from android device...");
  $('#error_overlay').hide();
  $.get("service", {'method' : 'input'}).done(function ( data ) {
	  if( console && console.log ) {
	    console.log("Sample of data:"+data);
	  }
	  if (data.input != undefined) {
		  if (data.input == "next") {
			  nextSlide();
		  } else {
			  prevSlide();
		  }
	  } else if (data.set != undefined) {
		  curSlide = data.set;
		  updateSlides();
	  }
	  android_getInput();
	  //console.log(slideEls[curSlide].getElementsByTagName("NOTES")[0].innerHTML);
	}).error(function() {
		//alert('Connection lost'); // or whatever
		$("#error_overlay").show();
  });
}

function android_send_slide(slide, slide_id) {
	var notes = "";
	if (slide.getElementsByTagName("NOTES").length > 0) {
		notes = slide.getElementsByTagName("NOTES")[0].innerHTML;
		console.log(notes);
	}
	else {
		notes = "Slide has no nodes";
		console.log("Slide has no nodes");
	}
	$.get("service", {'method' : 'slideinfo', 'notes' : notes, 'slide' : slide_id, 'slide_count' : slideEls.length}).error(function() {
	    //alert('woops'); // or whatever
	});
}

function android_Connect() {
  addStyleSheet("css/own.css", "all");
  addStyleSheet("google/stylesprint.css", "print");
  addStyleSheet("css/ownprint.css", "print");
  setTimeout(android_getInput, 2000);
  $(document).bind('slideenter', function(event) {
	  android_send_slide(getSlideEl(event.originalEvent.slideNumber-1), event.originalEvent.slideNumber);
  });
}

function addStyleSheet(name, type) {
  var el = document.createElement('link');
  el.rel = 'stylesheet';
  el.type = 'text/css';
  if (type == undefined)
    el.media = "screen";
  else
    el.media = type;
  el.href = name;
  document.body.appendChild(el);
}
