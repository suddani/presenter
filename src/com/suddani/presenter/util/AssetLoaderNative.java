package com.suddani.presenter.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.util.Log;

public class AssetLoaderNative implements AssetLoader {
	private String assetPath;
	private String downloadPath;

	public AssetLoaderNative(String assetPath, String downloadPath) {
		this.assetPath = assetPath;
		this.downloadPath = downloadPath;
	}
	public InputStream getResource(String name) throws IOException {
		Log.d("AssetLoader", "Load: "+name);
		File f = new File(assetPath+name);
		return new FileInputStream(f);
	}
	
	public InputStream getRealResource(String name) throws FileNotFoundException {
		Log.d("AssetLoader", "getRealResource: "+name);
		File f = new File(downloadPath+name);
		return new FileInputStream(f);
	}
}
