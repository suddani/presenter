package com.suddani.presenter.util;

import com.suddani.presenter.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class Notifyer {
	
	Context mContext;
	Intent mIntent;
	public String tickerText;
	public String contentTitle;
	public String contentText;
	
	int ID;
	
	static int notifyerID = 0;
	
	public Notifyer(int id, Context c, Intent i) {
		mContext = c;
		mIntent = i;
		ID = id;
		tickerText = "Hello";
		contentTitle = "My notification";
		contentText = "Hello World!";
	}
	
	public Notifyer(Context c, Intent i) {
		mContext = c;
		mIntent = i;
		tickerText = "Hello";
		contentTitle = "My notification";
		contentText = "Hello World!";
		ID = -1;
	}
	
	public Notifyer(String ticker, String title, String content, Context c, Intent i) {
		mContext = c;
		mIntent = i;
		tickerText = ticker;
		contentTitle = title;
		contentText = content;
		ID = -1;
	}
	
	public void stop() {
		if (ID == -1)
			return;
        String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager mNotificationManager = (NotificationManager) mContext.getSystemService(ns);
		mNotificationManager.cancel(ID);
		ID = -1;
	}
	
	public void start(boolean autoclose, boolean fix) {
		
		//Creating a notification
        int icon = R.drawable.ic_launcher;        // icon from resources
        //CharSequence tickerText = "Hello";              // ticker-text
        long when = System.currentTimeMillis();         // notification time
        Context context = mContext.getApplicationContext();      // application Context
        //CharSequence contentTitle = "My notification";  // expanded message title
        //CharSequence contentText = "Hello World!";      // expanded message text

        Intent notificationIntent = mIntent;//new Intent(mContext, ChooseCity.class);
        PendingIntent contentIntent = mIntent != null ? PendingIntent.getActivity(mContext, 0, notificationIntent, 0) : null;

        // the next two lines initialize the Notification, using the configurations above
        Notification notification = new Notification(icon, tickerText, when);
        notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
        notification.defaults |= Notification.DEFAULT_SOUND;
        if (autoclose)
        	notification.flags |= Notification.FLAG_AUTO_CANCEL;
        if (fix)
        	notification.flags |= Notification.FLAG_ONGOING_EVENT;
        
        //notify
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(ns);
        
        if (ID == -1)
        	ID = notifyerID++;
        mNotificationManager.notify(ID, notification);
	}
}

