package com.suddani.presenter.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

public class AssetLoaderAndroid implements AssetLoader {
	Context context;
	
	public AssetLoaderAndroid(Context ctx) {
		context = ctx;
	}
	
	public InputStream getResource(String name) throws IOException {
		Log.d("AssetLoader", "Load: "+name);
		return context.getAssets().open("html"+name);
	}
	
	public InputStream getRealResource(String name) throws FileNotFoundException {
		Log.d("AssetLoader", "getRealResource: "+name);
		File f = new File(Environment.getExternalStorageDirectory()+"/Download"+name);
		return new FileInputStream(f);
	}
}
