package com.suddani.presenter.util;

import com.suddani.presenter.PresentActivity;
import com.suddani.presenter.R;

import android.os.Handler;
import android.widget.TextView;

public class InterfaceUpdaterAndroid implements InterfaceUpdater {
	PresentActivity activity;
	Handler handler;
	
	class SetInterface implements Runnable {

		private PresentActivity activity;
		private String notes;
		private String slideId;
		private String slide_count;

		public SetInterface(PresentActivity activity, String notes, String slideId, String slide_count) {
			this.activity = activity;
			this.notes = notes;
			this.slideId = slideId;
			this.slide_count = slide_count;
		}

		@Override
		public void run() {
			((TextView)this.activity.findViewById(R.id.page)).setText(slideId+"/"+slide_count);
			((TextView)this.activity.findViewById(R.id.notes)).setText(notes);
		}
		
	}
	
	public InterfaceUpdaterAndroid(PresentActivity presentActivity, Handler handler) {
		this.activity = presentActivity;
		this.handler = handler;
	}

	@Override
	public void sendUpdate(String notes, String slideId, String slide_count) {
		handler.post(new SetInterface(activity, notes, slideId, slide_count));
	}

}
