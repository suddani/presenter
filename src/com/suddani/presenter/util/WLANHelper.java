package com.suddani.presenter.util;

import android.content.Context;
import android.net.wifi.WifiManager;

public class WLANHelper {
	public static String getIP(Context ctx) {
		WifiManager wifiManager = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
	    int ipAddress = wifiManager.getConnectionInfo().getIpAddress();
	    String formatedIpAddress = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff),
	        (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
	    return formatedIpAddress;
	}
}
