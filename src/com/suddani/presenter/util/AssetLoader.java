package com.suddani.presenter.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.CharBuffer;
import java.util.Hashtable;
import java.util.Map;

import android.util.Log;

public interface AssetLoader {
	/*
	static Map<String, String> resourceFiles = new Hashtable<String, String>();
	static Map<String, InputStream> resources = new Hashtable<String, InputStream>();
	
	public static String getHTMLResource(String name, Context context) throws IOException {
		if (resourceFiles.containsKey(name))
			return resourceFiles.get(name);

		StringBuffer strContent = new StringBuffer("");
		
		InputStreamReader instream = new InputStreamReader(context.getAssets().open(name));
		
		CharBuffer readBuf = CharBuffer.allocate(512 * 1024);
		
		int readCnt = instream.read(readBuf);
		while (0 < readCnt) {
			strContent.append(readBuf.array(), 0, readCnt);
			readCnt = instream.read(readBuf);
		}

		instream.close();
		
		String rsc = strContent.toString();
		resourceFiles.put(name, rsc);
		Log.d("AssetLoader", rsc);
		return rsc;
	}
	
	public static InputStream getImageResource(String name, Context context) throws IOException {
		return context.getAssets().open(name);
	}
	*/
	
	/*
	public static InputStream getResource(String name, Context context) throws IOException {
		Log.d("AssetLoader", "Load: "+name);
		return context.getAssets().open(name);
	}
	
	public static InputStream getRealResource(String name) throws FileNotFoundException {
		Log.d("AssetLoader", "getRealResource: "+name);
		File f = new File(Environment.getExternalStorageDirectory()+name);
		return new FileInputStream(f);
	}
	*/
	
	public InputStream getResource(String name) throws IOException;
	
	public InputStream getRealResource(String name) throws FileNotFoundException;
}
