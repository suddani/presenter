package com.suddani.presenter.server;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import com.suddani.presenter.service.PresenterState;
import com.suddani.presenter.util.AssetLoader;

import android.util.Log;

public abstract class MyServer extends NanoHTTPD {
	AssetLoader assetLoader;
	List<PresenterState> states;

	public MyServer(int port, AssetLoader assetLoader) throws IOException {
		super(port, null);
		this.assetLoader = assetLoader;
		states = new LinkedList<PresenterState>();
		Log.d("NANOHTTPD", "created Http server");
	}

	@Override
	public Response serve(String uri, String method, Properties header, Properties parms, Properties files) {
		Log.d("NANOHTTPD", uri);
		
		
		Set<Object> states = header.keySet(); // get set-view of keys 
		Iterator<Object> itr = states.iterator(); 
		while(itr.hasNext()) { 
			String key = (String) itr.next();
			Log.d("NANOHTTPD", "Property["+key+"]: "+header.getProperty(key));
		}
		
		
		if (uri.contains("/service"))
			return new NanoHTTPD.Response(HTTP_OK, MIME_JSON, jsonservice(uri, parms));
		
		PresenterState presenter = getPresenterID(header);
		
		return createResourceResponse(uri, presenter);
	}

	private PresenterState getPresenterID(Properties header) {
		String cookiestring = header.getProperty("cookie");
		if (cookiestring == null)
			return null;
		
		String cookies[] = cookiestring.split(";");
		
		for (String cookie: cookies) {
			if (cookie.contains("__presenter=")) {
				int id = -1;
				try {
					id = Integer.parseInt(cookie.split("=")[1]);
				} catch(NumberFormatException e) {
					
				}
				if (id < 0 || id >= states.size())
					return null;
				return states.get(id);
			}
		}
		return null;
	}

	public abstract String jsonservice(String uri, Properties parms);

	private Response createResourceResponse(String uri, PresenterState presenter) {
		String path = uri.matches("/") ? uri+"index.html" : uri;
		if (!path.matches(".*\\.\\w+"))
			return createPresentationResponse(path);//path += "/index.html";
		
		//if the session has no presentation assosiated with it cancel
		if (presenter == null || !uri.contains(presenter.presentation))
			return new NanoHTTPD.Response(HTTP_NOTFOUND, MIME_HTML, "This is not a valid session");
		
		
		//first try serving files from the presentation dir
		if (uri.contains(presenter.presentation)) {
			try {
				return new NanoHTTPD.Response(HTTP_OK, getMime(uri), assetLoader.getRealResource(uri));
			} catch (IOException e) {
				Log.d("Presentation", "failed to server: "+uri);
			}
		}
		
		//if that didn't work serve from assets
		path = uri.replace(presenter.presentation+"/", "");
		
		
		try {
			return new NanoHTTPD.Response(HTTP_OK, getMime(path), assetLoader.getResource(path));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				return new NanoHTTPD.Response(HTTP_NOTFOUND, "image/png", assetLoader.getResource("/404-error-template.jpg"));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return new NanoHTTPD.Response(HTTP_NOTFOUND, MIME_HTML, "Could not find resource");
			}
		}
		
	}

	private Response createPresentationResponse(String path) {
		Log.d("NANOHTTPD", "Create Presentation for: "+path);
		if (!path.matches("/\\w+/"))
			return new NanoHTTPD.Response(HTTP_NOTFOUND, MIME_HTML, "Invalid presentation path");
		String presentation = path.split("/")[1];
		try {
			NanoHTTPD.Response r = new NanoHTTPD.Response(HTTP_OK, MIME_HTML, assetLoader.getRealResource("/"+presentation+"/index.html"));
			states.add(new PresenterState(presentation));
			//r.addHeader("Set-Cookie", "__presenter="+(states.size()-1)+";");
			r.addHeader("Set-Cookie", "__presenter="+(states.size()-1)+";");
			return r;
		} catch (FileNotFoundException e) {
			Log.d("NANOHTTPD", e.toString());
		}
		return new NanoHTTPD.Response(HTTP_NOTFOUND, MIME_HTML, "Could not find presentation");
	}

	private String getMime(String path) {
		if (path.contains(".js"))
			return MIME_JS;
		else if (path.contains(".css"))
			return MIME_CSS;
		else if (path.contains(".png"))
			return MIME_PNG;
		else if (path.contains(".jpg"))
			return MIME_JPEG;
		return MIME_HTML;
	}

}
