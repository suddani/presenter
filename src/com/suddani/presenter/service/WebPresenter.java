package com.suddani.presenter.service;

import java.io.IOException;
import java.util.Properties;

import com.suddani.presenter.server.MyServer;
import com.suddani.presenter.util.AssetLoader;
import com.suddani.presenter.util.InterfaceUpdater;

import android.util.Log;

public class WebPresenter extends MyServer {

	String input_answer = "{\"input\":\"next\"}";
	Object sync_input;
	InterfaceUpdater updater;

	public WebPresenter(int port, AssetLoader assetLoader, Object sync_input, InterfaceUpdater updater) throws IOException {
		super(port, assetLoader);
		this.sync_input = sync_input;
		this.updater = updater;
	}

	//@Override
	public String jsonservice(String uri, Properties parms) {
		if (parms.getProperty("method") != null) {
			if (parms.getProperty("method").matches("input")) {
				input_answer = "{\"none\":\"none\"}";
				return collectInput();
			} else if (parms.getProperty("method").matches("slideinfo")) {
				Log.d("WebPresenter", "recevied slide info");
				handleSlideInfo(parms.getProperty("notes"), parms.getProperty("slide"), parms.getProperty("slide_count"));
			}
		}
		return "{\"lol\":\""+parms.getProperty("lol")+"\"}";
	}

	private void handleSlideInfo(String notes, String slideId, String slide_count) {
		updater.sendUpdate(notes, slideId, slide_count);
	}

	private String collectInput() {
		
		synchronized (sync_input) {
			try {
				Log.d("WebPresenter", "waiting for input");
				sync_input.wait();
				Log.d("WebPresenter", "received for input");
			} catch (InterruptedException e) {
				Log.d("WebPresenter", e.toString());
			}
		}
		return input_answer;
	}

	synchronized public void showNextSlide() {
		input_answer = "{\"input\":\"next\"}";
		synchronized (sync_input) {
			sync_input.notifyAll();
		}
	}

	synchronized public void showPrevSlide() {
		input_answer = "{\"input\":\"prev\"}";
		synchronized (sync_input) {
			sync_input.notifyAll();
		}
	}

}
