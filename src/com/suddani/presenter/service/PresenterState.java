package com.suddani.presenter.service;

import android.util.Log;

public class PresenterState {
	public String presentation;
	public PresenterState(String name) {
		presentation = name;
		Log.d("PresenterState", "created new state: "+name);
	}
}
