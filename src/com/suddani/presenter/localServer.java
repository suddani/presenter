package com.suddani.presenter;

import java.io.FileInputStream;
import java.io.IOException;

import com.suddani.presenter.service.WebPresenter;
import com.suddani.presenter.util.AssetLoaderNative;
import com.suddani.presenter.util.InterfaceUpdaterNative;

public class localServer {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException {
		String current = new java.io.File( "." ).getCanonicalPath();
        System.out.println("Current dir:"+current);

        String html_base = args.length >= 3 ? args[1] : "../git/presenter/assets/html/";
        String serve_dir = args.length >= 3 ? args[2] : "/home/dsudmann/desktop/";
        
		Object sync_input = new Object();
		WebPresenter presenter = new WebPresenter(8888, new AssetLoaderNative(html_base, serve_dir), sync_input, new InterfaceUpdaterNative());
		synchronized(sync_input) {
			sync_input.wait();
		}
	}

}