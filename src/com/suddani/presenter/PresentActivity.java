package com.suddani.presenter;

import java.io.IOException;

import com.suddani.presenter.service.WebPresenter;
import com.suddani.presenter.util.AssetLoaderAndroid;
import com.suddani.presenter.util.InterfaceUpdaterAndroid;
import com.suddani.presenter.util.Notifyer;
import com.suddani.presenter.util.WLANHelper;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;

public class PresentActivity extends Activity {

	private WebPresenter server;
	private Notifyer notify;
	private Object sync_input;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_present);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		startServer();		
	}
	
	private void startServer() {
		sync_input = new Object();
		try {
			Log.d("NANOHTTPD", "try creating server");
			server = new WebPresenter(8080, new AssetLoaderAndroid(this), sync_input, new InterfaceUpdaterAndroid(this, new Handler()));
			notify = new Notifyer("Started Presenter", "Presenter Adress", "Go to http://"+WLANHelper.getIP(this)+": 8080/", this, null);
			notify.start(false, true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			Log.d("NANOHTTPD", "creating server failed bc: "+e.toString());
		}
	}

	public void onDestroy() {
		synchronized(sync_input) {
			sync_input.notifyAll();
		}
		server.stop();
		notify.stop();
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_present, menu);
		return true;
	}
	
	public void nextSlide(View v) {
		Log.d("PresentActivity", "next");
		server.showNextSlide();
	}
	
	public void prevSlide(View v) {
		Log.d("PresentActivity", "prev");
		server.showPrevSlide();
	}

}
